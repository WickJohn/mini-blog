import { put, call, all, takeLatest } from 'redux-saga/effects';
import {
  FETCH_POSTLIST_REQUEST,
  FETCH_POSTLIST_SUCCESS,
  FETCH_COMMENTS_REQUEST,
  FETCH_COMMENTS_SUCCESS,
  FETCH_POST_REQUEST,
  FETCH_POST_SUCCESS,
 } from './actionTypes';


const BASE_URL = 'https://jsonplaceholder.typicode.com'

const api = (url) => fetch(`${BASE_URL}${url}`).then(res=> res.json().then(result => result))

function *fetchPostList() {
  const posts = yield call(api, '/posts')
  yield put({ type: FETCH_POSTLIST_SUCCESS, data : posts })
}

function *watcherFetchPostList() {
  yield takeLatest(FETCH_POSTLIST_REQUEST, fetchPostList)
}



function *fetchComments(action) {
  const comments = yield call(api, `/comments?postId=${action.id}`)
  yield put({ type: FETCH_COMMENTS_SUCCESS, data : comments})
}

function *watcherFetchComments() {
  yield takeLatest(FETCH_COMMENTS_REQUEST, fetchComments)
}

function *fetchPost(action) {
  const post = yield call(api, `/posts/${action.id}`)
  const user = yield call(api, `/users/${post.userId}`)
  yield put({ type: FETCH_POST_SUCCESS, data: {post, user}})
}

function *watcherFetchPost() {
  yield takeLatest(FETCH_POST_REQUEST, fetchPost)
}





 export default function *rootSaga() {
  yield all([
    watcherFetchPostList(),
    watcherFetchComments(),
    watcherFetchPost(),
  ])
}
