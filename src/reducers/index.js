import {
  FETCH_POSTLIST_SUCCESS,
  FETCH_POST_SUCCESS,
  FETCH_COMMENTS_SUCCESS,
} from '../actionTypes';


 const defaultState = {
  postList: [],
  post : {},
  user : {},
  comments : [],
}

const blogReducer = (state=defaultState, action) => {
  switch(action.type) {
    case FETCH_POSTLIST_SUCCESS :
      return {...state, postList : [...action.data]}
    case FETCH_COMMENTS_SUCCESS :
      return {...state, comments : [...action.data]}
    case FETCH_POST_SUCCESS :
      return {...state, post : {...action.data.post}, user : {...action.data.user}}
    default :
      return state
  }
  console.log('action', action)
}

export default blogReducer;
