import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import "regenerator-runtime/runtime";
import storeConfig from './storeConfig';
import MainPage from './components/page/MainPage';
import DetailPage from './components/page/DetailPage';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';
import './main.css';


const store = storeConfig();

const  baseUrl = process.env.PUBLIC_URL;

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <div className="App">
        <Route  exact path={baseUrl + "/"} component={MainPage} />
        <Route exact path='/post/:id' component={DetailPage} />
      </div>
    </Router>
  </Provider>,
  document.getElementById("index")
);
