import { combineReducers } from 'redux';
import blogReducer from './reducers'

const rootReducer = combineReducers({
  blogReducer,
})

export default rootReducer;
