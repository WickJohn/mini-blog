import React from 'react';


const Search = ({ onChange, value, onSubmit, children }) => (
  <form onSubmit={onSubmit}>
    {children}
    <input type="text" onChange={onChange} value={value}/>
  </form>
)

export default Search
