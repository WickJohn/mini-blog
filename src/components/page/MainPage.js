import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchPostsListRequest } from '../../actions';
import Search from '../Search';


const isSearched = query => post => (
  post.title.toLowerCase().includes(query.toLowerCase())
)


const  baseUrl = process.env.PUBLIC_URL;

class MainPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      query : "",
    }
  }

  componentDidMount() {
      this.props.fetchPosts()
  }


  onSearchChange = event => {
    this.setState({ query : event.target.value })
  }

  onSearchSubmit = (e) => {
    e.preventDefault();
  }

 renderPosts() {
   const { posts } = this.props;
      return (
      posts.filter(isSearched(this.state.query)).map( post => (
      <Link key={post.id} to={`/post/${post.id}`}>
        <div key={post.id} className='table-row'>
          <h1 className="table-header">{post.title}</h1>
        </div>
      </Link>
    ))
  )
  }

  render() {
    return (
      <div>
        <Search
          onChange={this.onSearchChange}
          onSubmit={this.onSearchSubmit}
          value={this.state.query}>
          Search
        </Search>
        <div className="table">
          {this.renderPosts()}
        </div>
      </div>
    );
  };
}


const mapStateToProps = state => ({
  posts : state.blogReducer.postList
})

const mapToDispatch = dispatch => ({
  fetchPosts : () => dispatch(fetchPostsListRequest())
})
export default connect(mapStateToProps, mapToDispatch)(MainPage);
