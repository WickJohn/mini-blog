import React, {Component} from 'react';
import { connect } from 'react-redux';
import {
   fetchCommentsRequest,
   fetchPostRequest,
  } from '../../actions';


class DetailPage extends Component {
  componentDidMount() {
      this.props.fetchPost(this.props.match.params.id)
      this.props.fetchComments(this.props.match.params.id)
  }

   renderComments() {
     const {comments} = this.props;
     return (
    <div>
      {comments.map( comment => (
        <div key={comment.id} className="comment">
          <div>
            <p>{comment.name}</p>
            <p>{comment.email}</p>
          </div>
          <p>{comment.body}</p>
        </div>
      ))}
    </div>
  )

}
  render() {
    const { post, user } = this.props;
    return (
      <div className="post-container">
        <div className="userInfo">
          <p>{user.name}</p>
          <p>{user.username}</p>
          <p>{user.email}</p>
        </div>
      <div className="post">
          <h1>{post.title}</h1>
          <p>{post.body}</p>
      </div>
      <div className="commentList">
        Comments :
        {this.renderComments()}
      </div>
    </div>
    )
  }
}

const mapStateToProps = state => ({
  post : state.blogReducer.post,
  comments : state.blogReducer.comments,
  user : state.blogReducer.user,
})

const mapToDispatch = dispatch => ({
  fetchComments : id => dispatch(fetchCommentsRequest(id)),
  fetchPost : id => dispatch(fetchPostRequest(id)),
})


export default connect(mapStateToProps, mapToDispatch)(DetailPage)
