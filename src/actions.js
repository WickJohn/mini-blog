import {
  FETCH_POSTLIST_REQUEST,
  FETCH_COMMENTS_REQUEST,
  FETCH_POST_REQUEST,
  FETCH_USER_REQUEST,
} from './actionTypes'

export const fetchPostsListRequest = () => ({
  type: FETCH_POSTLIST_REQUEST
})

export const fetchCommentsRequest = id => ({
  type: FETCH_COMMENTS_REQUEST,
  id
})

export const fetchPostRequest = id => ({
  type : FETCH_POST_REQUEST,
  id
})
